# Created by use_targets().
# Follow the comments below to fill in this target script.
# Then follow the manual to check and run the pipeline:
#   https://books.ropensci.org/targets/walkthrough.html#inspect-the-pipeline

# Load packages required to define the pipeline:
library(targets)
library(future)
# library(tarchetypes) # Load other packages as needed.

future::plan("multisession")

# Set target options:
tar_option_set(
  packages = c("tidyverse","mFD","gtools","hillR") # packages that your targets need to run
  # format = "qs", # Optionally set the default storage format. qs is fast.
  #
  # For distributed computing in tar_make(), supply a {crew} controller
  # as discussed at https://books.ropensci.org/targets/crew.html.
  # Choose a controller that suits your needs. For example, the following
  # sets a controller with 2 workers which will run as local R processes:
  #
  #   controller = crew::crew_controller_local(workers = 2)
  #
  # Alternatively, if you want workers to run on a high-performance computing
  # cluster, select a controller from the {crew.cluster} package. The following
  # example is a controller for Sun Grid Engine (SGE).
  #
  #   controller = crew.cluster::crew_controller_sge(
  #     workers = 50,
  #     # Many clusters install R as an environment module, and you can load it
  #     # with the script_lines argument. To select a specific verison of R,
  #     # you may need to include a version string, e.g. "module load R/4.3.0".
  #     # Check with your system administrator if you are unsure.
  #     script_lines = "module load R"
  #   )
  #
  # Set other options as needed.
)

# tar_make_clustermq() is an older (pre-{crew}) way to do distributed computing
# in {targets}, and its configuration for your machine is below.
#options(clustermq.scheduler = "multicore")

# tar_make_future() is an older (pre-{crew}) way to do distributed computing
# in {targets}, and its configuration for your machine is below.
# Install packages {{future}}, {{future.callr}}, and {{future.batchtools}} to allow use_targets() to configure tar_make_future() options.

# Run the R scripts in the R/ folder with your custom functions:
tar_source()
# source("other_functions.R") # Source other scripts as needed.

# Replace the target list below with your own:
list(
  tar_target(file_otu_asmb,   "test_data/seqtab_norm.tsv.gz", format = "file"),
  tar_target(file_otu_trait,  "test_data/trait_predicted.tsv.gz", format = "file"),
  tar_target(asmb_otu,        read_otu_asmb_data(file_otu_asmb)),
  tar_target(otu_trait,       read_otu_trait_data(file_otu_trait)),
  tar_target(dummy_trait_cat, build_dummy_trait_cat(otu_trait)),

  tar_target(hill_indices,  compute_hillnum_indices(asmb_otu)),
  tar_target(fun_ab_indice, compute_fun_ab(asmb_otu,otu_trait)),

  tar_target(mfd_fe,         compute_mfd_fe(otu_trait,dummy_trait_cat)),
  tar_target(mfd_fe_indices, compute_fe_indices(asmb_otu,mfd_fe)),
  tar_target(output_fe,      output_fe_indices(mfd_fe_indices)),
  tar_target(asmb_fe,        build_asmb_fe(asmb_otu,mfd_fe)),

  tar_target(mfd_fspace,              build_functional_space(mfd_fe,ndim = 5)),
  tar_target(mfd_fspace_indices_fdis, compute_fspace_indices(asmb_fe,mfd_fspace,"fdis")),
  tar_target(mfd_fspace_indices_fdiv, compute_fspace_indices(asmb_fe,mfd_fspace,"fdiv")),
  tar_target(mfd_fspace_indices_fmpd, compute_fspace_indices(asmb_fe,mfd_fspace,"fmpd")),
  tar_target(mfd_fspace_indices_fnnd, compute_fspace_indices(asmb_fe,mfd_fspace,"fnnd")),
  tar_target(mfd_fspace_indices_feve, compute_fspace_indices(asmb_fe,mfd_fspace,"feve")),
  tar_target(mfd_fspace_indices_fric, compute_fspace_indices(asmb_fe,mfd_fspace,"fric")),
  tar_target(mfd_fspace_indices_fori, compute_fspace_indices(asmb_fe,mfd_fspace,"fori")),
  tar_target(mfd_fspace_indices_fspe, compute_fspace_indices(asmb_fe,mfd_fspace,"fspe")),

  tar_target(output_fspace_indices_fdis, output_fspace_indices(mfd_fspace_indices_fdis)),
  tar_target(output_fspace_indices_fdiv, output_fspace_indices(mfd_fspace_indices_fdiv)),
  tar_target(output_fspace_indices_fmpd, output_fspace_indices(mfd_fspace_indices_fmpd)),
  tar_target(output_fspace_indices_fnnd, output_fspace_indices(mfd_fspace_indices_fnnd)),
  tar_target(output_fspace_indices_feve, output_fspace_indices(mfd_fspace_indices_feve)),
  tar_target(output_fspace_indices_fric, output_fspace_indices(mfd_fspace_indices_fric)),
  tar_target(output_fspace_indices_fori, output_fspace_indices(mfd_fspace_indices_fori)),
  tar_target(output_fspace_indices_fspe, output_fspace_indices(mfd_fspace_indices_fspe)),

  tar_target(mfd_fdist, build_fdist(mfd_fe,dummy_trait_cat)),
  tar_target(mfd_fhill_indices_tau_min,  compute_fhill_indices(asmb_fe,mfd_fdist,"min")),
  tar_target(mfd_fhill_indices_tau_mean, compute_fhill_indices(asmb_fe,mfd_fdist,"mean")),
  tar_target(mfd_fhill_indices_tau_max,  compute_fhill_indices(asmb_fe,mfd_fdist,"max")),

  tar_target(output_fhill_indices_tau_min,  output_fhill_indices(mfd_fhill_indices_tau_min)),
  tar_target(output_fhill_indices_tau_mean, output_fhill_indices(mfd_fhill_indices_tau_mean)),
  tar_target(output_fhill_indices_tau_max,  output_fhill_indices(mfd_fhill_indices_tau_max))
)
