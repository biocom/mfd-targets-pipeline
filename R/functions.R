# INPUT ----

read_otu_trait_data <- function(file) {
  dat <- read_tsv(file)
  dat |> column_to_rownames(names(dat)[1])
}

read_otu_asmb_data <- function(file) {
  dat <- read_tsv(file)
  dat |>
    pivot_longer(cols = -1) |>
    pivot_wider(values_from = "value", names_from = names(dat)[1]) |>
    column_to_rownames("name")
}

build_dummy_trait_cat <- function(otu_trait){
  tibble(trait_name = colnames(otu_trait),
         trait_type = "Q")
}

# FE ----

compute_mfd_fe <- function(otu_trait, trait_cat){
  sp.to.fe(sp_tr  = otu_trait,
           tr_cat = trait_cat)
}

compute_fe_indices <- function(asmb_otu, mfd_fe){
  alpha.fd.fe(asb_sp_occ = (asmb_otu != 0),
              sp_to_fe   = mfd_fe)
}

output_fe_indices <- function(asmb_fdfe_ind){
  as_tibble(asmb_fdfe_ind$asb_fdfe, rownames = "asmb") |>
    arrange(asmb)
}

build_asmb_fe <- function(asmb_otu,mfd_fe){
  fe_order <-  mfd_fe$sp_fe |> unname() |> gtools::mixedsort() |> unique()
  link_fe_otu <- tibble(fe = forcats::fct_relevel(mfd_fe$sp_fe, fe_order) ,
                        otu = names(mfd_fe$sp_fe))
  asmb_otu |>
    t() |>
    as.data.frame() |>
    rownames_to_column("otu") |>
    inner_join(link_fe_otu, by = "otu") |>
    pivot_longer(-c(otu,fe)) |>
    group_by(fe,name) |>
    summarise(value = sum(value), .groups = "drop") |>
    pivot_wider(names_from = fe, values_from = value) |>
    column_to_rownames("name") |>
    as.matrix()
}

# FHILL ----

build_fdist <- function( mfd_fe, trait_cat){
  mFD::funct.dist(sp_tr  = mfd_fe$fe_tr,
                  tr_cat = trait_cat,
                  metric = "euclidean")
}

compute_fhill_indices <- function(asmb_fe, mfd_fdist, tau){
  res <- mFD::alpha.fd.hill(asb_sp_w = asmb_fe,
                     sp_dist  = mfd_fdist,
                     tau      = tau,
                     q        = c(0,1,2))
  res$tau <- tau
  return(res)
}

output_fhill_indices <- function(mfd_fdfhill_ind){
  mfd_fdfhill_ind$asb_FD_Hill |>
    as_tibble(rownames = "asmb") |>
    rename_with(~ str_replace(.,"FD_",paste0("fhill_",mfd_fdfhill_ind$tau,"_")))

}

# FSPACE ----

build_functional_space <- function(mfd_fe,ndim = 5){
      mFD::tr.cont.fspace(
        sp_tr   = mfd_fe$fe_tr,
        nb_dim  = ndim,  #15 max
        scaling = "center")
}

#c("fdis", "fmpd", "fnnd", "feve", "fric", "fdiv", "fori", "fspe")

compute_fspace_indices <- function(asmb_fe, mfd_fspace, indice){
    mFD::alpha.fd.multidim(
      sp_faxes_coord   = mfd_fspace$sp_faxes_coord,
      asb_sp_w         = asmb_fe,
      ind_vect         = indice,
      scaling          = TRUE,
      check_input      = TRUE,
      details_returned = TRUE,
      verbose = FALSE)
}

output_fspace_indices <- function(asmb_fdfspace_ind){
  as_tibble(asmb_fdfspace_ind$functional_diversity_indices, rownames = "asmb") |>
    arrange(asmb) |>
    select(-sp_richn)
}

# MISC ----

compute_hillnum_indices <- function(asmb_otu){
  tibble(asmb = rownames(asmb_otu),
         hill_q0 = hillR::hill_taxa(asmb_otu, q = 0),
         hill_q1 = hillR::hill_taxa(asmb_otu, q = 1),
         hill_q2 = hillR::hill_taxa(asmb_otu, q = 2))
}

compute_fun_ab <- function(asmb_otu, otu_trait){
  #filter otu in otu x trait matrix but not asmb x otu matrix
  otu_to_keep <- rownames(otu_trait) %in% colnames(asmb_otu)
  asmb_trait <- as.matrix(asmb_otu) %*% as.matrix(filter(otu_trait,otu_to_keep))
  #row sum the asmb x trait matrix to get count
  tibble(asmb = rownames(asmb_otu),
         fun_ab = rowSums(asmb_trait))
}

# DEMO ----
#
# library(tidyverse)
# library(mFD)
# library(hillR)
# source("R/sp_to_fe.R")
#
# # load data
# res_otu_trait <- read_otu_trait_data("data_2/KO_predicted.SMALLER.tsv.gz")
# res_asmb_otu <- read_otu_asmb_data("data_2/seqtab_norm.tsv.gz")
# res_trait_cat <- build_dummy_trait_cat(res_otu_trait)
#
# # fe
# res_mfd_fe <- compute_mfd_fe(res_otu_trait,res_trait_cat)
# res_fe_indices <- compute_fe_indices(res_asmb_otu,res_mfd_fe)
# output_fe_indices(res_fe_indices)
#
# # asmb x fe
# res_asmb_fe <- build_asmb_fe(res_asmb_otu,res_mfd_fe)
#
# # fspace
# # #c("fdis", "fmpd", "fnnd", "feve", "fric", "fdiv", "fori", "fspe", "fide")
# res_mfd_fspace <- build_functional_space(res_mfd_fe)
# res_fspace_indices <- compute_fspace_indices(res_asmb_fe,res_mfd_fspace,"fdis")
# output_fspace_indices(res_fspace_indices)
#
# # fhill
# res_mfd_dist <- build_fdist(res_mfd_fe,res_trait_cat)
# res_fd_fh_indices <- compute_fhill_indices(res_asmb_fe, res_mfd_dist,"mean")
# output_fhill_indices(res_fd_fh_indices)
#
# # misc
# compute_fun_ab(res_asmb_otu,res_otu_trait)
# compute_hillnum_indices(res_asmb_otu)
